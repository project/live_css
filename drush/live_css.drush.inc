<?php
/**
 * @file
 * Action for DruSH to take to easily set up Live CSS and download Ace
 * for the user.
 */
define('ACE_DOWNLOAD_URI', 'https://github.com/ajaxorg/ace-builds/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function live_css_drush_command() {
  $items['ace-download'] = array(
    'callback' => 'live_css_drush_download',
    'description' => dt('Downloads the required ACE library from http://ace.c9.io/.'),
    'arguments' => array(
      'path' => dt('Optional. The path to the download folder. If omitted, Drush will use the default location (<code>sites/all/libraries/ace</code>).'),
    ),
  );
  return $items;
}

/**
 * Downloads
 */
function live_css_drush_download() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/all/libraries/ace';
  }
  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive
  $filepath = drush_download_file(ACE_DOWNLOAD_URI);
  if ($filepath) {
    $filename = basename($filepath);

    // Remove any existing Ace plugin directory
    if (is_dir('ace-builds-master')) {
      drush_delete_dir('ace-builds-master', TRUE);
      drush_log(dt('An existing Ace plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive
    drush_tarball_extract($filename, '../');
    drush_move_dir('../ace-builds-master', '../ace', TRUE);
  }

  if (is_dir('../ace')) {
    drush_log(dt('Ace plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Ace plugin to @path', array('@path' => $path)), 'error');
  }
  // Set working directory back to the previous working directory.
  chdir($olddir);
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_live_css_post_enable() {
  $modules = func_get_args();
  if (in_array('live_css', $modules)) {
    live_css_drush_download();
  }
}
