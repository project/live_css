(function ($) {
  var $body = $('body'),
    storage = {
      hasStorage: window.localStorage ? true : false,
      curFile: '',
      setCursor: function (pos) {
        if (!this.hasStorage) {
          return;
        }
        localStorage.livecss_row = pos.row + 1;
        localStorage.livecss_column = pos.column;
      },
      getCursor: function () {
        if (!this.hasStorage) {
          return {
            row: 0,
            column: 0
          };
        } else {
          return {
            row: localStorage.livecss_row || 0,
            column: localStorage.livecss_column || 0
          };
        }
      },
      setFile: function (file) {
        if (!this.hasStorage) {
          return;
        }

        if (localStorage.livecss_file == file) {
          return;
        }
        localStorage.livecss_file = file;
        localStorage.livecss_row = 0;
        localStorage.livecss_column = 0;
      },
      getFile: function () {
        if (!this.hasStorage) {
          return '';
        } else {
          return localStorage.livecss_file;
        }
      }
    };

  $(document).ready(function () {
    storage.hasStorage = storage.hasStorage && Drupal.settings.liveCSS.storage;
  });

  //main css styler management object
  window.styler = {
    html: "<div id='css-controls'><select id='css-stylelist' /><div id='css-save'>Save</div><div id='css-close'>Close</div><div id='css-dock'>&#x2B07;</div></div>",
    styleObjs: {},
    sList: [],
    init: function () {

      //create ui
      $('#csidebar').append(this.html);

      var self = this;

      self.autoSave.init();

      $('#css-stylelist').on({
        focus: function () {
          if (self.autoSave.switchSave === true) {
            self.autoSave.oldPath = $(this).val();
            self.autoSave.oldCSS = editor.getText();
          }
        },
        change: function () {
          self.selectStyle($(this).val(), function (css) {
            if (self.autoSave.switchSave === true) {
              self.saveCSS(css);
            } else {
              editor.setText(css);
            }
          });
          storage.setFile($(this).val());
        }
      });
      $('#css-save').click(function () {
        self.saveCSS();
      });
      $('#css-close').click(function () {
        sidebar.remove();
      });

      $('#css-dock').html((sidebar.dock == 'right') ? '&#x2B07;' : '&#x2B06;');

      $('#css-dock').click(function () {
        sidebar.dock = sidebar.dock == 'right' ? 'bottom' : 'right';
        sidebar.dock = sidebar.dock;
        sidebar.remove();
        sidebar.show();
        $('#csstab').css('display', 'none');
        styler.init();
      });


      this.loadStyleSheets();

      function refresh(styleObj, css) {
        styleObj.setStyle(css);
      }

      self.selectStyle($('#css-stylelist').val(), function (css) {
        //create the editor
        editor.init({
          css: css,
          change: function () {
            if (self.styleObjs[self.curStyle] != null) refresh(self.styleObjs[self.curStyle], editor.getText());
          },
          setCursor: function (pos) {
            storage.setCursor(pos);
          },
          startPos: storage.getCursor()
        });
      });
    },
    loadStyleSheets: function () {
      var addItem = function (item) {

        // Remove query string
        var title = item.split('?')[0];

        // Remove http://, https://, ftp://, //, etc.
        title = title.replace(/^.*\/\//, '');

        // Normalize path to base url
        var parts = title.split('/');
        title = '';
        var rootIndex = Drupal.settings.basePath.split('/').length - 1;
        for (var i = rootIndex; i < parts.length; i++) {
          title = title + parts[i] + '/';
        }
        title = title.substr(0, title.length - 1);

        // Limit to just the custom css if necessary
        if (Drupal.settings.liveCSS.pathGoggles !== '') {
          if (title.search(new RegExp(Drupal.settings.liveCSS.pathGoggles)) < 0) {
            return;
          }
        }

        // Limit to just theme css if necessary
        if (Drupal.settings.liveCSS.hideModules && Drupal.settings.liveCSS.pathGoggles === '') {
          if (title.search(/sites\/[A-Za-z0-9.]+\/themes\//) < 0) {
            return;
          }
        }

        // Ignore google fonts
        //  - This should be generalised to ignore all external styles as they are not editable
        if (item.search(/googleapis/) > -1) {
          return;
        }

        // Only enable less stylesheets if less is enabled
        if (!Drupal.settings.liveCSS.less) {
          if (title.search(/\.less/) > -1) {
            return;
          }
        }

        // Make this stylesheet load automatically if storage usage
        // enabled and matches.
        if (item == storage.getFile()) {
          $('#css-stylelist').append('<option selected="selected" value="' + item + '">' + title + '</option>');
        } else {
          $('#css-stylelist').append('<option value="' + item + '">' + title + '</option>');
        }
      };
      if (this.sList.length === 0) {
        var self = this;

        // Add <link> elements
        $('link').each(function () {
          if (this.type == 'text/css' || this.rel == 'stylesheet') {
            addItem(this.href);
            self.sList.push(this.href);
          }

        });

        // Add <style> elements
        $('style').each(function () {
          var styles = $(this).html().match(/@import url\(['"](.*)['"]\)/gi);
          if (styles) for (var i = 0; i < styles.length; i++) {
            var item = styles[i].substring(13, styles[i].length - 2);
            addItem(item);
            self.sList.push(item);
          }
        });
      } else {
        for (var i = 0; i < this.sList.length; i++)
        addItem(this.sList[i]);
      }
    },
    saveCSS: function (cssNew) {
      var self = this,
        cssNew = cssNew || editor.getText(),
        css = (self.autoSave.oldCSS !== '') ? self.autoSave.oldCSS : cssNew,
        href = (self.autoSave.oldPath !== '') ? self.autoSave.oldPath : this.curStyle;

      $('#css-save').html('...');

      $.ajax({
        type: 'POST',
        url: Drupal.settings.liveCSS.savePath,
        data: {
          css: css,
          href: href
        },
        dataType: "json",
        complete: function (response) {
          var res = JSON.parse(response.responseText);
          if (self.autoSave.switchSave) {
            editor.setText(cssNew);
            self.autoSave.oldPath = '';
            self.autoSave.oldCSS = '';
          }
          $('#css-save').html('Save');
          if (res.result != "success") {
            alert(res.msg);
          }
        }
      });

    },
    selectStyle: function (href, complete) {
      //update selected
      this.curStyle = href;
      if (this.styleObjs[href] == null) {

        if ( !! window.less && href.search(/\.less/i) > -1) {
          //remove the less styles
          var parts = href.split('/');
          var lessId = [];
          for (var i = 3; i < parts.length; i++)
          lessId.push(parts[i]);

          lessId = lessId.join('-');
          lessId = 'less:' + lessId.substring(0, lessId.length - 5);
          $('style[id="' + lessId + '"]').remove();

          //store the less code
        }

        this.styleObjs[href] = new style(href, complete);
      } else {
        complete(this.styleObjs[href].getStyle());
      }
    },
    autoSave: {
      switchSave: false,
      oldPath: '',
      oldCSS: '',
      init: function () {
        var self = this;
        if (Drupal.settings.liveCSS.switchSave) {
          $('#css-controls').append("<div id='css-switch'><input id='css-autosave' name='css-autosave' type='checkbox' /><label for='css-autosave' title='Enabling will save CSS every time a new file is selected.'>Switch/Save</label></div>");
          $('#css-autosave').on('change', function (e) {
            if (storage.hasStorage) {
              localStorage.livecss_switchSave = this.checked;
            }
            self.switchSave = this.checked;
            self.oldPath = '';
            self.oldCSS = '';
          });
          if (localStorage.livecss_switchSave == "true") {
            $('#css-autosave').prop('checked', true);
            self.switchSave = true;
          } else {
            self.switchSave = false;
          }
        }
      }
    }
  };

  //sidebar ui setup
  window.sidebar = {
    html: "<div id='csidebar'></div>",
    dock: 'right',
    init: function () {

      // Edit button
      var edittab = $("<div id='csstab'><div>Edit CSS</div></div>");
      $body.append(edittab);

      var self = this;

      edittab.click(function () {
        self.show();
        edittab.css('display', 'none');
        styler.init();
      });
    },
    show: function () {

      $body.parent().append(this.html);
      var self = this;

      //disable typical scrollbars
      $body.parent().css('overflow', 'hidden');

      $body.css({
        'position': 'relative',
          'overflow': 'scroll',
          'min-width': 0
      });

      $('#csidebar').addClass(sidebar.dock);

      //hide admin menu
      if ($('#toolbar, #admin-menu').length > 0 && Drupal.settings.liveCSS.hideAdmin == 1) {
        $('#toolbar, #admin-menu').css('display', 'none');
        $body.removeClass('toolbar admin-menu');
        if (Drupal.settings.liveCSS.menuMargin == 1) {
          $body.css('margin-top', '');
        }
      }

      var size = function () {
        h = $(window).height();
        w = $(window).width();


        if (sidebar.dock == 'right') {
          //set the body size
          $body.css('width', w - 500);
          $body.css('height', h - parseInt($body.css('paddingTop'), 10) - parseInt($body.css('marginTop'), 10));

          //set the sidebar size
          $('#csidebar').css('height', h);

          //editor size
          editor.height(h);
          editor.width(494);
        } else if (sidebar.dock == 'bottom') {
          $body.css('width', w - parseInt($body.css('paddingLeft'), 10) - parseInt($body.css('marginTop'), 10));
          $body.css('height', h - 300);

          $('#csidebar').css('width', w);

          //editor size
          editor.height(300);
          editor.width(w - 6);
        }
      };
      $(window).on('resize', size);
      size();
    },
    remove: function () {
      $('#csidebar').remove();
      $body.parent().css('overflow', 'auto');
      $body.css({
        position: 'static',
        overflow: 'visible',
        width: 'auto',
        height: 'auto'
      });

      $(window).off('resize');

      $('#csstab').css('display', 'block');

      // put the admin_menu back
      if ($('#toolbar, #admin-menu').length > 0) {
        $('#toolbar, #admin-menu').css('display', 'block');
        $body.addClass('toolbar admin-menu');
        if (Drupal.settings.liveCSS.menuMargin == 1) {
          $body.css('margin-top', '29px');
        }
      }
    }
  };

  //class for generating stylesheet hooks from a style url

  var urlRegEx = /@import\s*("([^"]*)"|'([^']*)')|url\s*\(\s*(\s*"([^"]*)"|'([^']*)'|[^\)]*\s*)\s*\)/ig;

  window.style = function (href, complete) {
    this.href = href;
    var self = this;

    if ( !! window.less) {
      if (window.style.lessParser === undefined) {
        /*options = {
    optimization: ...,
    paths: [],
    mime: ...
  }*/
        window.style.lessParser = new less.Parser();
      }
    }
    var hideLess = function (href) {
      var parts = href.split('/');
      var idStr = '';
      for (var i = 3; i < parts.length - 1; i++)
      idStr += parts[i] + '-';

      idStr += parts[parts.length - 1].split('.')[0];
      idStr = 'less:' + idStr;

      var lessEl = $('style[id="' + idStr + '"]');

      lessEl.remove();
    };

    //given an href, attach the style and return the getter and setter

    //full uri conversion for css asset loading
    this.fullUri = function (css) {
      var href = self.href;
      var parts = href.split('/');
      var baseURI = [];
      var dots = '';
      while (parts.pop()) {
        baseURI.push(parts.join('/'));

        var result;
        while (result = urlRegEx.exec(css)) {
          url = result[3] || result[2] || result[5] || result[6] || result[4];

          if (dots.length ? url.substr(0, dots.length) == dots : url.substr(0, 1) != '/') {
            var quoteLen = result[5] || result[6] ? 1 : 0;
            var newUrl = parts.join('/') + '/' + url.substr(dots.length);
            css = css.substr(0, urlRegEx.lastIndex - url.length - quoteLen - 1) + newUrl + css.substr(urlRegEx.lastIndex - quoteLen - 1);
            urlRegEx.lastIndex = urlRegEx.lastIndex + (newUrl.length - url.length);
          }
        }

        dots += '../';
      }
      return css;
    };

    //short uri backwards conversion for css asset loading
    this.shortUri = function (css) {
      var href = self.href;
      var parts = href.split('/');
      var baseURI = [];
      var dots = '';
      while (parts.pop()) {
        baseURI.push(parts.join('/'));

        var result;
        while (result = urlRegEx.exec(css)) {
          url = result[3] || result[2] || result[5] || result[6] || result[4];

          var partPrefix = parts.join('/') + '/';

          if (url.substr(0, partPrefix.length) == partPrefix) {
            var quoteLen = result[5] || result[6] ? 1 : 0;
            var newUrl = dots + url.substr(partPrefix.length);
            css = css.substr(0, urlRegEx.lastIndex - url.length - quoteLen - 1) + newUrl + css.substr(urlRegEx.lastIndex - quoteLen - 1);
            urlRegEx.lastIndex = urlRegEx.lastIndex + (newUrl.length - url.length);
          }
        }

        css = css.replace(new RegExp('url\\(' + parts.join('/') + '/', 'gi'), 'url(' + dots);
        dots += '../';
      }
      return css;
    };

    //check the links and styles
    $('link, style').each(function () {
      var element = this;

      //for link elements
      if (this.href == href) {
        //insert this new stylesheet after the link element
        self.link = $("<style type='text/css'/>");
        self.link.type = "text/css";
        $(this).after(self.link);

        //load the stylesheet data and insert it into the new style element
        $.get(href, function (data) {
          self.setStyle(data);
          complete(data);
        });

        self.setStyle = function (css) {

          if ( !! window.style.lessParser && this.href.search(/\.less/i) > -1) {
            hideLess(this.href);
            this.less = css;
            try {
              window.style.lessParser.parse(css, function (e, tree) {
                css = tree.toCSS();
              });
            } catch (e) {}
          }

          self.link.html(self.fullUri(css));
        };
        self.getStyle = function () {
          if (this.href.search(/\.less$/i) > -1 && this.less !== undefined) return this.less;
          else return self.shortUri(self.link.html());
        };

        //leave the loop
        return false;
      }

      //for style elements
      var html = $(this).html();
      if (html.indexOf(href) > -1) {

        self.setStyle = function (css) {
          var href = self.href;
          href = href.replace(/\./g, '\\.');
          href = href.replace(/\//g, '\\/');
          href = href.replace(/\?/g, '\\?');
          var exp = new RegExp("\\/\\*\\+" + href + "\\*\\/(.|\\n|\\r)*\\/\\*\\-" + href + "\\*\\/");

          if ( !! window.style.lessParser && this.href.match(/\.less/i)) {
            hideLess(this.href);
            this.less = css;
            try {
              window.style.lessParser.parse(css, function (e, tree) {
                css = tree.toCSS();
              });
            } catch (e) {}
          }

          $(element).html($(element).html().replace(exp, '/*+' + self.href + '*/' + self.fullUri(css) + '/*-' + self.href + '*/'));
        };

        self.getStyle = function () {
          if (this.href.match(/\.less/i) && this.less !== undefined) return this.less;

          var href = self.href;
          href = href.replace(/\./g, '\\.');
          href = href.replace(/\//g, '\\/');
          href = href.replace(/\?/g, '\\?');
          var exp = new RegExp("\\/\\*\\+" + href + "\\*\\/(.|\\n|\\r)*\\/\\*\\-" + href + "\\*\\/");

          var style = $(element).html().match(exp)[0];
          return self.shortUri(style.substring(self.href.length + 5, style.length - self.href.length - 5));
        };

        if (/@import/gi.test(html) === true) {
          var html = $(this).html();

          //extract the full import list
          var files = html.match(/@import url\(['"]((.|\n|\r)*?)['"]\);/gi);

          //replace the @import codes with comment markers
          $(this).html(html.replace(/@import url\(['"]((.|\n|\r)*?)['"]\);/gi, '/*+$1*//*-$1*/\n\r'));

          //download all the import files for replacement into the markers
          $(files).each(function () {
            var filename = this.substring(13, this.length - 3);
            $.get(filename, function (data) {
              var href = filename;
              href = href.replace(/\./g, '\\.');
              href = href.replace(/\//g, '\\/');
              href = href.replace(/\?/g, '\\?');

              var exp = new RegExp("\\/\\*\\+" + href + "\\*\\/(.|\\n|\\r)*\\/\\*\\-" + href + "\\*\\/");
              $(element).html($(element).html().replace(exp, '/*+' + filename + '*/' + data + '/*-' + filename + '*/'));

              //run the complete function when we replace the one we wanted
              if (filename == self.href) {
                complete(data);
              }
            });
          });
        } else {
          //styles already loaded
          complete(self.getStyle());
        }

        return false;
      }

      return true;
    });
  };

  window.editor = {
    html: "<div id='cedit'></div>",
    startLine: 0,
    startCol: 0,
    editor: null,
    change: null,
    init: function (o) {
      $('#csidebar').append(this.html);

      $('#cedit').css('height', $(window).height() - ($('#cedit').offset().top - $('#csidebar').offset().top));

      var self = this;

      $('#cedit').html(o.css);

      var editor = ace.edit('cedit');
      var CSSMode = require("ace/mode/css").Mode;
      if (Drupal.settings.liveCSS.theme) editor.setTheme("ace/theme/" + Drupal.settings.liveCSS.theme);
      editor.getSession().setMode(new CSSMode());

      editor.setShowPrintMargin(false);
      editor.setHighlightActiveLine(false);
      editor.getSession().setTabSize(Drupal.settings.liveCSS.tabSize);
      editor.getSession().setUseSoftTabs(Drupal.settings.liveCSS.softTabs);
      $('#cedit').css('font-size', Drupal.settings.liveCSS.fontSize);

      editor.commands.addCommand({
        name: "save",
        bindKey: {
          win: "Ctrl-S",
          mac: "Command-S",
          sender: "editor|cli"
        },
        exec: function () {
          styler.saveCSS();
        }
      });
      editor.commands.addCommand({
        name: "close",
        bindKey: {
          win: "Esc",
          mac: "Esc",
          sender: "editor"
        },
        exec: function () {
          sidebar.remove();
        }
      });
      //add the change event
      if (o.change) {
        editor.getSession().on('change', o.change);
      }

      editor.getSession().selection.on('changeCursor', function () {
        var pos = editor.getSession().getSelection().getCursor();
        o.setCursor(pos);
      });

      this.editor = editor;
    },
    width: function (sWidth) {
      //set editor width
      $('#cedit').css('width', sWidth);
    },
    height: function (sHeight) {
      if (sidebar.dock == 'right') $('#cedit').css('height', sHeight - 65);
      else $('#cedit').css('height', sHeight - 40);
    },
    getText: function () {
      return this.editor.getSession().getValue();
    },
    setText: function (css) {
      this.editor.getSession().setValue(css);
      var self = this;
    }
  };

  $(document).ready(function () {
    sidebar.init();
  });

})(jQuery);
