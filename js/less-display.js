(function($) {
  $('link').each(function() {
    if ($(this).attr('href').search(/\.less/i) > -1)
      $(this).attr('rel', 'stylesheet/less');
  });
})(jQuery);
